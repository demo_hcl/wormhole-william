module github.com/psanford/wormhole-william


go 1.12

require (
	github.com/cheggaaa/pb/v3 v3.0.8
	github.com/gorilla/websocket v1.5.0
	github.com/klauspost/compress v1.15.0
	github.com/mdp/qrterminal/v3 v3.0.0
	github.com/spf13/cobra v1.1.3
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	nhooyr.io/websocket v1.8.7
	salsa.debian.org/vasudev/gospake2 v0.0.0-20210510093858-d91629950ad1
)
